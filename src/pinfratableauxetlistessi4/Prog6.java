package pinfratableauxetlistessi4;

public class Prog6 {
    
    public static void main(String[] args){
        String[] fruits = {"pomme", "poire", "abricot", "ananas", "citron",
                           "peche", "mirabelle", "fraise", "framboise",
                           "raisin", "groseille", "prune", "orange", "banane"};
        
        for (int i=0; i<fruits.length; i++) {
            if (fruits[i].length()==6) {
                System.out.println(fruits[i]);
            }
        }
    }
}
