package pinfratableauxetlistessi4;

public class Prog3 {
    
    public static void main(String[] args) {
        float[] notes = {12, 8, 14, 7, 9.5f, 14, 15.5f, 6, 16, 11, 14, 10.5f};
        float max=0.0f, min=20.0f, moy=0.0f, total=0.0f;
        
        for (int i=0; i<notes.length; i++) {
            if (notes[i]>max) {
                max=notes[i];
            }
            if (notes[i]<min) {
                min=notes[i];
            }
            total=total+notes[i];
        }
        moy=total/notes.length;
        System.out.println("Note max : "+max);
        System.out.println("Note min : "+min);
        System.out.println("Moyenne : "+moy);
    }
}
