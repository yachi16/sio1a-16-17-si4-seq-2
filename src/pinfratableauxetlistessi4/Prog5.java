package pinfratableauxetlistessi4;

public class Prog5 {
    public static void main(String[] args) {
        int[] tab = {33, 56, 14, 76, 81, 2, 57, 13, 61, 64, 45, 35, 95, 15};
        int moyP=0, moyI=0, totalP=0, totalI=0, comptP=0, comptI=0;
        
        for (int i=0; i<tab.length; i++) {
            if (tab[i]%2==0) {
                comptP++;
                totalP+=tab[i];
            }
            if (tab[i]%2!=0) {
                comptI++;
                totalI+=tab[i];
            }
        }
        
        moyP=totalP/comptP;
        moyI=totalI/comptI;
        
        System.out.println("Moyenne des chiffres pair : "+moyP);
        System.out.println("Moyenne des chiffres impair : "+moyI);   
    }
}
