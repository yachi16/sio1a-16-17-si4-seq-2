package AchiSIO1A1617SI4seq2;

import java.util.Arrays;
import java.util.List;

public class Prog8 {
    
    public static void main(String[] args) {
       
        String alphaClair = " ABCDEFGHIJKLMNOPQRSTUVWXYZO123456789";
        
        List<String> alphaMorse = Arrays.asList( "  ",".-","-...","-.-.","-..",".","..-.","--.",
                                                 "....","..",".---",
                                                 "-.-",".-..","--","-.","---","-.","--.-",".-.","...","-",
                                                 "..-","...-",".--","-..-","-.--","--..",
                                                 ".----","..---","...--","....-",".....",
                                                 "-....","--...","---..","---.","-----"
                                                );
                
        String messDetresse="SOS TITANIC SINKING BY THE HEAD WWE ARE ABOUT ALL DOWN";
        
        for (int i=0; i<messDetresse.length(); i++) {
            char c = messDetresse.charAt(i);
            int index = alphaClair.indexOf(c);
            System.out.print(alphaMorse.get(index));
        }
        
        System.out.println(""); 
    }
}
